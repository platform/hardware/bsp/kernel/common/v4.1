Rockchip platforms device tree bindings
---------------------------------------

- Kylin RK3036 board:
    Required root node properties:
      - compatible = "rockchip,kylin-rk3036", "rockchip,rk3036";

- MarsBoard RK3066 board:
    Required root node properties:
      - compatible = "haoyu,marsboard-rk3066", "rockchip,rk3066a";

- bq Curie 2 tablet:
    Required root node properties:
      - compatible = "mundoreader,bq-curie2", "rockchip,rk3066a";

- ChipSPARK Rayeager PX2 board:
    Required root node properties:
      - compatible = "chipspark,rayeager-px2", "rockchip,rk3066a";

- Radxa Rock board:
    Required root node properties:
      - compatible = "radxa,rock", "rockchip,rk3188";

- Firefly Firefly-RK3288 board:
    Required root node properties:
      - compatible = "firefly,firefly-rk3288", "rockchip,rk3288";
    or
      - compatible = "firefly,firefly-rk3288-beta", "rockchip,rk3288";

- ChipSPARK PopMetal-RK3288 board:
    Required root node properties:
      - compatible = "chipspark,popmetal-rk3288", "rockchip,rk3288";
